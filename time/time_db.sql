-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 10, 2019 at 12:24 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `time_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, NULL, NULL, 'Casio'),
(2, NULL, NULL, 'Adidas'),
(3, NULL, NULL, 'Dooka'),
(4, NULL, NULL, 'Valentino'),
(5, NULL, NULL, 'Weide'),
(6, NULL, NULL, 'Zoo York'),
(7, NULL, NULL, 'Chronomart'),
(8, NULL, NULL, 'Daniel Klein');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, NULL, NULL, 'Digital Watches'),
(2, NULL, NULL, 'Analog Watches'),
(3, NULL, NULL, 'Sports Watches');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE `genders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `created_at`, `updated_at`, `name`) VALUES
(1, NULL, NULL, 'Women'),
(2, NULL, NULL, 'Men');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `stocks` bigint(20) DEFAULT NULL,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png',
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `gender_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `created_at`, `updated_at`, `name`, `description`, `price`, `stocks`, `image_url`, `category_id`, `gender_id`, `brand_id`) VALUES
(5, '2019-05-28 01:58:33', '2019-05-28 19:06:25', 'Adidas District 2', 'Adidas District 2 Analog Watch', '1900.00', 10, 'images/1559037512.jpg', 2, 1, 2),
(6, '2019-05-28 02:00:22', '2019-05-28 18:13:09', 'Adidas Process M1', 'Adidas Process M1 Analog Watch', '1500.00', 17, 'images/1559037622.jpg', 2, 1, 2),
(7, '2019-05-28 02:01:22', '2019-05-28 20:07:10', 'Casio AOD', 'Casio AOD Analog Watch', '2100.00', 5, 'images/1559037682.jpg', 2, 2, 1),
(8, '2019-05-28 02:02:50', '2019-05-28 18:13:09', 'Adidas Process M3', 'Adidas Process M3 Analog Watch', '1600.00', 49, 'images/1559037770.jpg', 2, 1, 2),
(9, '2019-05-28 02:03:55', '2019-05-28 22:52:26', 'Adidas SP1', 'Adidas SP1 Analog Watch', '2000.00', 21, 'images/1559037835.jpg', 2, 1, 2),
(10, '2019-05-28 02:05:03', '2019-05-28 02:05:03', 'Casio MTP Silver', 'Casio MTP  Silver Analog Watch', '2500.00', 31, 'images/1559037903.jpg', 2, 2, 1),
(11, '2019-05-28 02:06:47', '2019-05-28 02:06:47', 'Casio W600F', 'Casio W600F Analog Watch', '2400.00', 45, 'images/1559038007.jpg', 2, 2, 1),
(12, '2019-05-28 02:08:05', '2019-05-28 02:08:05', 'Casio G-Shock', 'Casio G-Shock Black Analog Watch', '4000.00', 10, 'images/1559038085.jpg', 2, 2, 1),
(13, '2019-05-28 02:09:16', '2019-05-28 02:09:16', 'Chronomart Lace Fabric', 'Chronomart Lace Fabric Analog Watch', '2900.00', 20, 'images/1559038156.jpg', 2, 1, 7),
(14, '2019-05-28 02:10:07', '2019-05-28 22:52:37', 'Chronomart Nafisa Peony', 'Chronomart Nafisa Peony 3D Analog Watch', '5000.00', 19, 'images/1559038207.jpg', 2, 1, 7),
(15, '2019-05-28 02:11:54', '2019-05-28 02:11:54', 'Chronomart Nafisa Blue', 'Chronomart Nafisa Blue Analog Watch', '4800.00', 10, 'images/1559038314.jpg', 2, 1, 7),
(16, '2019-05-28 02:12:54', '2019-05-28 22:52:53', 'Chronomart Nafisa', 'Chronomart Nafisa  Analog Watch', '4000.00', 5, 'images/1559038374.jpg', 2, 1, 7),
(17, '2019-05-28 02:14:08', '2019-05-28 02:14:08', 'Adidas Legend Marine', 'Adidas Legend Marine Digital Watch', '4000.00', 21, 'images/1559038448.jpg', 1, 1, 2),
(18, '2019-05-28 02:15:29', '2019-05-28 02:15:29', 'Casio A321', 'Casio A321 Digital Watch', '2300.00', 12, 'images/1559038529.jpg', 1, 1, 1),
(19, '2019-05-28 02:16:22', '2019-05-28 02:16:22', 'Casio A8138', 'Casio A8138 Digital Watch', '3000.00', 41, 'images/1559038582.jpg', 1, 1, 1),
(20, '2019-05-28 02:16:52', '2019-05-28 02:16:52', 'Casio LA69', 'Casio LA69 Digital Watch', '2600.00', 15, 'images/1559038612.jpg', 1, 1, 1),
(21, '2019-05-28 02:17:49', '2019-05-28 02:17:49', 'Casio LA69WEM', 'Casio LA69WEM Digital Watch', '2500.00', 14, 'images/1559038669.jpg', 1, 1, 1),
(22, '2019-05-28 02:18:27', '2019-05-28 02:18:27', 'Casio Square LA', 'Casio Square LA Digital Watch', '2400.00', 17, 'images/1559038707.jpg', 1, 1, 1),
(23, '2019-05-28 02:19:01', '2019-05-28 02:19:01', 'Weide 5C Blue', 'Weide 5C Blue Digital Watch', '5000.00', 17, 'images/1559038741.jpg', 1, 2, 5),
(24, '2019-05-28 02:19:37', '2019-05-28 02:19:37', 'Weide Orange Index', 'Weide Orange Index Digital Watch', '6000.00', 54, 'images/1559038777.jpg', 1, 2, 5),
(25, '2019-05-28 02:26:19', '2019-05-28 02:26:19', 'Weide WA3J', 'Weide WA3J Digital Watch', '3500.00', 10, 'images/1559039179.jpg', 1, 2, 5),
(26, '2019-05-28 02:32:12', '2019-05-28 22:53:12', 'Weide White WA3', 'Weide White WA3 Digital Watch', '6200.00', 15, 'images/1559039532.jpg', 1, 2, 5),
(27, '2019-05-28 02:32:58', '2019-05-28 02:32:58', 'Zoo York Black Silicon', 'Zoo York Black Silicon Digital Watch', '6300.00', 80, 'images/1559039578.jpg', 1, 2, 6),
(28, '2019-05-28 02:33:57', '2019-05-28 02:33:57', 'Zoo York Camouflage', 'Zoo York Camouflage Digital Watch', '6400.00', 80, 'images/1559039637.jpg', 1, 2, 6),
(29, '2019-05-28 02:35:41', '2019-05-28 02:35:41', 'Zoo York Digital 51', 'Zoo York Digital 51 Digital Watch', '6000.00', 47, 'images/1559039741.jpg', 1, 2, 6),
(30, '2019-05-28 02:36:39', '2019-05-28 02:36:39', 'Chronomart Orkina Auto', 'Chronomart Orkina Auto Embosed', '6300.00', 69, 'images/1559039799.jpg', 2, 1, 7),
(31, '2019-05-28 02:37:58', '2019-05-28 02:37:58', 'Daniel Klein Dual Tone', 'Daniel Klein Dual Tone Stainless Analog Watch', '5000.00', 78, 'images/1559039878.jpg', 2, 1, 8),
(32, '2019-05-28 02:38:39', '2019-05-28 02:38:39', 'Daniel Klein DK42', 'Daniel Klein DK42 Analog Watch', '4000.00', 65, 'images/1559039919.jpg', 2, 1, 8),
(33, '2019-05-28 02:39:27', '2019-05-28 02:39:27', 'Daniel Klein Dual Tone A31', 'Daniel Klein Dual Tone A31 Analog Watch', '3700.00', 36, 'images/1559039967.jpg', 2, 1, 8),
(34, '2019-05-28 02:40:19', '2019-05-28 22:53:36', 'Daniel Klein Mesh', 'Daniel Klein Mesh Analog Watch', '5100.00', 12, 'images/1559040019.jpg', 2, 1, 8),
(35, '2019-05-28 02:40:52', '2019-05-28 02:40:52', 'Daniel Klein Mesh 41', 'Daniel Klein Mesh 41 Analog Watch', '3900.00', 45, 'images/1559040052.jpg', 2, 1, 8),
(36, '2019-05-28 02:41:31', '2019-05-28 02:41:31', 'Daniel Klein Rubber Mesh', 'Daniel Klein Rubber Mesh Analog Watch', '2900.00', 45, 'images/1559040091.jpg', 1, 1, 8),
(37, '2019-05-28 02:42:06', '2019-05-28 02:42:06', 'Daniel Klein Silver Mesh', 'Daniel Klein Silver Mesh Analog Watch', '3700.00', 58, 'images/1559040126.jpg', 2, 1, 8),
(38, '2019-05-28 02:42:46', '2019-05-28 02:42:46', 'Valentino HL45', 'Valentino HL45 Analog Watch', '4100.00', 12, 'images/1559040166.jpg', 2, 1, 4),
(39, '2019-05-28 02:43:48', '2019-05-28 02:43:48', 'Dooka 662', 'Dooka 662 Sports Watch', '5000.00', 10, 'images/1559040228.jpg', 3, 2, 3),
(40, '2019-05-28 02:44:25', '2019-05-28 02:44:25', 'Dooka 665', 'Dooka 665 Green Sport Watch', '4200.00', 14, 'images/1559040265.jpg', 3, 2, 3),
(41, '2019-05-28 02:44:55', '2019-05-28 02:44:55', 'Dooka 667', 'Dooka 667 Sport Watch', '4000.00', 17, 'images/1559040295.jpg', 3, 2, 3),
(42, '2019-05-28 02:45:27', '2019-05-28 02:45:27', 'Dooka SWR', 'Dooka SWR Sports Watch', '4100.00', 78, 'images/1559040327.jpg', 1, 2, 3),
(43, '2019-05-28 02:46:03', '2019-05-28 02:46:03', 'Weide 2C BLK', 'Weide 2C BLK Black Sports Watch', '6800.00', 45, 'images/1559040363.jpg', 3, 2, 5),
(44, '2019-05-28 02:46:44', '2019-05-28 02:46:44', 'Weide 8C KHAKI', 'Weide 8C KHAKI Sports Watch', '6900.00', 45, 'images/1559040404.jpg', 3, 2, 5),
(45, '2019-05-28 02:47:22', '2019-05-28 02:47:22', 'Weide WSH 1C', 'Weide WSH 1C Sports Watch', '6300.00', 68, 'images/1559040442.jpg', 3, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `item_order`
--

CREATE TABLE `item_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_28_031040_create_genders_table', 1),
(4, '2019_05_28_031158_update_gender_table', 1),
(5, '2019_05_28_031214_create_categories_table', 1),
(6, '2019_05_28_031237_update_categories_table', 1),
(7, '2019_05_28_031255_create_brands_table', 1),
(8, '2019_05_28_031327_update_brands_table', 1),
(9, '2019_05_28_031408_create_items_table', 1),
(10, '2019_05_28_031434_update_items_table', 1),
(11, '2019_05_28_031501_create_statuses_table', 1),
(12, '2019_05_28_031529_update_statuses_table', 1),
(13, '2019_05_28_031555_create_orders_table', 1),
(14, '2019_05_28_031619_update_orders_table', 1),
(15, '2019_05_28_055759_create_item_order_table', 1),
(16, '2019_05_28_060028_add_columns_item_order_table', 1),
(17, '2019_05_30_123734_add_column_to_orders_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total` decimal(10,2) NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/profile-default.png',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `image_url`, `email`, `email_verified_at`, `is_admin`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jaspher', 'images/1560099925.jpg', 'jaspher@gmail.com', NULL, 1, '$2y$10$jwKCHYyaB/YQeiuIW3D.Pev/zb289K3gtHYcsX9dPhJR2BE.olXG.', NULL, '2019-06-09 08:40:00', '2019-06-09 09:05:25'),
(2, 'user', 'images/1560099162.jpg', 'user@gmail.com', NULL, NULL, '$2y$10$RZJ.hDDsYGbB2nXs5glWkecBb6vWglO8sacIpXZJgAnbhDo5yvM1a', NULL, '2019-06-09 08:40:24', '2019-06-09 08:52:42'),
(3, 'user2', 'images/profile-default.png', 'user2@gmail.com', NULL, NULL, '$2y$10$wdcd7YU.2fUhuRs3MOlp4uy.MkUAmIa67VqphUv3vbFWF9x7vgRi6', NULL, '2019-06-09 08:40:41', '2019-06-09 08:40:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_category_id_foreign` (`category_id`),
  ADD KEY `items_gender_id_foreign` (`gender_id`),
  ADD KEY `items_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `item_order`
--
ALTER TABLE `item_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_order_item_id_foreign` (`item_id`),
  ADD KEY `item_order_order_id_foreign` (`order_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_status_id_foreign` (`status_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `genders`
--
ALTER TABLE `genders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `item_order`
--
ALTER TABLE `item_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `items_gender_id_foreign` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `item_order`
--
ALTER TABLE `item_order`
  ADD CONSTRAINT `item_order_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `item_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
