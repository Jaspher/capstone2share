<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->string('name');
            $table->text('description');
            $table->decimal('price',10,2);
            $table->bigInteger('stocks')->nullable();
            $table->string('image_url')->default('https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png');
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('gender_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            

            $table->foreign('category_id')
            ->references('id')
            ->on('categories')
            ->onDelete('set null')
            ->onUpdate('cascade');

            $table->foreign('gender_id')
            ->references('id')
            ->on('genders')
            ->onDelete('set null')
            ->onUpdate('cascade');        

            $table->foreign('brand_id')
            ->references('id')
            ->on('brands')
            ->onDelete('set null')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            //
        });
    }
}
