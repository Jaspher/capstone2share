<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('orders')->delete();
        
        \DB::table('orders')->insert(array (
            0 => 
            array (
                'id' => 2,
                'created_at' => '2019-05-28 06:06:35',
                'updated_at' => '2019-05-28 18:04:46',
                'total' => '3000.00',
                'user_id' => 2,
                'status_id' => 3,
            ),
            1 => 
            array (
                'id' => 3,
                'created_at' => '2019-05-28 06:25:44',
                'updated_at' => '2019-05-29 04:13:36',
                'total' => '9000.00',
                'user_id' => 2,
                'status_id' => 1,
            ),
            2 => 
            array (
                'id' => 4,
                'created_at' => '2019-05-28 16:57:00',
                'updated_at' => '2019-05-28 16:57:02',
                'total' => '19200.00',
                'user_id' => 2,
                'status_id' => 1,
            ),
            3 => 
            array (
                'id' => 5,
                'created_at' => '2019-05-28 17:05:58',
                'updated_at' => '2019-05-28 17:05:59',
                'total' => '1800.00',
                'user_id' => 2,
                'status_id' => 1,
            ),
            4 => 
            array (
                'id' => 6,
                'created_at' => '2019-05-28 17:10:09',
                'updated_at' => '2019-05-28 18:04:54',
                'total' => '0.00',
                'user_id' => 2,
                'status_id' => 2,
            ),
            5 => 
            array (
                'id' => 7,
                'created_at' => '2019-05-28 17:10:45',
                'updated_at' => '2019-05-29 04:04:34',
                'total' => '0.00',
                'user_id' => 2,
                'status_id' => 1,
            ),
            6 => 
            array (
                'id' => 8,
                'created_at' => '2019-05-28 17:11:05',
                'updated_at' => '2019-05-29 04:04:42',
                'total' => '0.00',
                'user_id' => 2,
                'status_id' => 2,
            ),
            7 => 
            array (
                'id' => 9,
                'created_at' => '2019-05-28 17:12:37',
                'updated_at' => '2019-05-28 17:56:14',
                'total' => '6200.00',
                'user_id' => 2,
                'status_id' => 3,
            ),
            8 => 
            array (
                'id' => 10,
                'created_at' => '2019-05-28 17:12:46',
                'updated_at' => '2019-05-28 18:22:08',
                'total' => '0.00',
                'user_id' => 2,
                'status_id' => 2,
            ),
            9 => 
            array (
                'id' => 11,
                'created_at' => '2019-05-28 17:14:48',
                'updated_at' => '2019-05-28 17:14:48',
                'total' => '1800.00',
                'user_id' => 2,
                'status_id' => 1,
            ),
            10 => 
            array (
                'id' => 12,
                'created_at' => '2019-05-29 02:10:13',
                'updated_at' => '2019-05-29 02:10:13',
                'total' => '2100.00',
                'user_id' => 2,
                'status_id' => 1,
            ),
            11 => 
            array (
                'id' => 13,
                'created_at' => '2019-05-29 02:13:09',
                'updated_at' => '2019-05-29 02:13:09',
                'total' => '12000.00',
                'user_id' => 2,
                'status_id' => 1,
            ),
            12 => 
            array (
                'id' => 14,
                'created_at' => '2019-05-29 04:07:10',
                'updated_at' => '2019-05-29 04:07:10',
                'total' => '6300.00',
                'user_id' => 1,
                'status_id' => 1,
            ),
        ));
        
        
    }
}