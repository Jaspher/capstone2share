<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'jaspher',
                'email' => 'jaspher@gmail.com',
                'email_verified_at' => NULL,
                'is_admin' => 1,
                'password' => '$2y$10$g3rlHQEsU50mzckKJY./wel1f9x1EhbHoGg/r4JbGv1skEK7FDIW6',
                'remember_token' => NULL,
                'created_at' => '2019-05-28 03:09:12',
                'updated_at' => '2019-05-28 03:09:12',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'user',
                'email' => 'user@gmail.com',
                'email_verified_at' => NULL,
                'is_admin' => NULL,
                'password' => '$2y$10$rCaNzWQZK26lZwh7zLuZdu0qZcshugAQYbqkv062V1jbr4oSX./FW',
                'remember_token' => NULL,
                'created_at' => '2019-05-28 06:05:38',
                'updated_at' => '2019-05-28 06:05:38',
            ),
        ));
        
        
    }
}