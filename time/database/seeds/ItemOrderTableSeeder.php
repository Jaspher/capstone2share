<?php

use Illuminate\Database\Seeder;

class ItemOrderTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('item_order')->delete();
        
        \DB::table('item_order')->insert(array (
            0 => 
            array (
                'id' => 4,
                'created_at' => '2019-05-28 16:57:01',
                'updated_at' => '2019-05-28 16:57:01',
                'item_id' => 7,
                'order_id' => 4,
                'quantity' => 2,
            ),
            1 => 
            array (
                'id' => 5,
                'created_at' => '2019-05-28 16:57:02',
                'updated_at' => '2019-05-28 16:57:02',
                'item_id' => 14,
                'order_id' => 4,
                'quantity' => 3,
            ),
            2 => 
            array (
                'id' => 6,
                'created_at' => '2019-05-28 17:05:58',
                'updated_at' => '2019-05-28 17:05:58',
                'item_id' => 5,
                'order_id' => 5,
                'quantity' => 1,
            ),
            3 => 
            array (
                'id' => 7,
                'created_at' => '2019-05-28 17:10:46',
                'updated_at' => '2019-05-28 17:10:46',
                'item_id' => 26,
                'order_id' => 7,
                'quantity' => 1,
            ),
            4 => 
            array (
                'id' => 8,
                'created_at' => '2019-05-28 17:11:06',
                'updated_at' => '2019-05-28 17:11:06',
                'item_id' => 26,
                'order_id' => 8,
                'quantity' => 1,
            ),
            5 => 
            array (
                'id' => 9,
                'created_at' => '2019-05-28 17:12:37',
                'updated_at' => '2019-05-28 17:12:37',
                'item_id' => 26,
                'order_id' => 9,
                'quantity' => 1,
            ),
            6 => 
            array (
                'id' => 10,
                'created_at' => '2019-05-28 17:14:48',
                'updated_at' => '2019-05-28 17:14:48',
                'item_id' => 5,
                'order_id' => 11,
                'quantity' => 1,
            ),
            7 => 
            array (
                'id' => 11,
                'created_at' => '2019-05-29 02:10:13',
                'updated_at' => '2019-05-29 02:10:13',
                'item_id' => 7,
                'order_id' => 12,
                'quantity' => 1,
            ),
            8 => 
            array (
                'id' => 12,
                'created_at' => '2019-05-29 02:13:09',
                'updated_at' => '2019-05-29 02:13:09',
                'item_id' => 6,
                'order_id' => 13,
                'quantity' => 3,
            ),
            9 => 
            array (
                'id' => 13,
                'created_at' => '2019-05-29 02:13:09',
                'updated_at' => '2019-05-29 02:13:09',
                'item_id' => 7,
                'order_id' => 13,
                'quantity' => 1,
            ),
            10 => 
            array (
                'id' => 14,
                'created_at' => '2019-05-29 02:13:09',
                'updated_at' => '2019-05-29 02:13:09',
                'item_id' => 8,
                'order_id' => 13,
                'quantity' => 1,
            ),
            11 => 
            array (
                'id' => 15,
                'created_at' => '2019-05-29 02:13:09',
                'updated_at' => '2019-05-29 02:13:09',
                'item_id' => 5,
                'order_id' => 13,
                'quantity' => 1,
            ),
            12 => 
            array (
                'id' => 16,
                'created_at' => '2019-05-29 02:13:09',
                'updated_at' => '2019-05-29 02:13:09',
                'item_id' => 9,
                'order_id' => 13,
                'quantity' => 1,
            ),
            13 => 
            array (
                'id' => 17,
                'created_at' => '2019-05-29 04:07:10',
                'updated_at' => '2019-05-29 04:07:10',
                'item_id' => 7,
                'order_id' => 14,
                'quantity' => 3,
            ),
        ));
        
        
    }
}