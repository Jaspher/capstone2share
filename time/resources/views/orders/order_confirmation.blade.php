{{-- {{ $order }} --}}
@extends('layouts.app');
@section('title','Order Confirmation')
@section('content')
<div class="container">
	<div class="jumbotron jumbotron-fluid order-confirmation-bg mt-3">
		<div class="container text-center">
			<h1 class="display-4 text-center">Thank you for shopping!</h1>

			<a class="text-center details order-history btn btn-outline-dark" href="{{ url('/orders') }}">Show order History</a>
		</div>
	</div>
</div> {{-- end cont --}}

@endsection