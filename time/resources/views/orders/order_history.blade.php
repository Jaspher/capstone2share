@extends('layouts.app')
@section('content')
<div class="container">
	<div class="jumbotron jumb-bg pt-3">
	<div class="row">	
		<div class="col-md-12">
			<table class="table table-hover details text-center  text-dark">
				<h1 class="text-center web-brand">ORDERS</h1>
				<thead>
					@auth
   						 @if(auth()->user()->is_admin == 1) 
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							
						@if(Session::has('restore_message'))
						<span class="order_span">{{Session::get('restore_message')}}</span>
						@elseif(Session::has('delete_message'))
						<span class="order_span">{{Session::get('delete_message')}}</span>
						<a class="order_span" href="{{Session::get('undo_url')}}">Undo</a>
						@endif

						</td>
					</tr>
					@endauth
					@endif
					<tr>
						<th>Order #</th>
						<th>Date Purchased</th>
						<th>Total</th>
						<th>Status</th>
						@auth
   						 
						<th>Action</th>
						
						@endauth	
					</tr>
				</thead>				
				<tbody>
					@foreach($orders as $order)					
					<tr>															
						<td>{{ $order->id }}</td>						
						<td>{{ $order->created_at }}</td>
						<td>₱{{ number_format($order->total,2) }}</td>
						<td>{{ $order->status->name }}</td>						
						@auth
   						 @if(auth()->user()->is_admin == 1) 
						<td >
							
								<form class="d-inline" action="/orders/{{ $order->id }}/cancel" method="POST" enctype="multipart/form-data">
									@csrf
									{{ method_field("PATCH") }}	
									<button type="submit" class="btn btn-danger">Cancel</button>
								</form>

								<form class="d-inline" action="/orders/{{ $order->id }}/pending" method="POST" enctype="multipart/form-data">
									@csrf
									{{ method_field("PATCH") }}									
									<button type="submit"  class="btn btn-primary">Pending</button>												
								</form>
								<form class="d-inline" action="/orders/{{ $order->id }}/completed" method="POST" enctype="multipart/form-data">
									@csrf
									{{ method_field("PATCH") }}									
									<button type="submit" class="btn btn-success">Completed</button>															
								</form>

								<a href=""  onclick="deleteOrder({{$order->id}})" class="btn btn-dark fas fa-trash-alt" data-toggle="modal" data-target="#delete_order"></a>
												
						</td>						
						@endif
						@endauth

						@if((Auth::user())&&(auth()->user()->is_admin == null))	 
						<td>
							<form class="d-inline" action="/orders/{{ $order->id }}/cancel" method="POST" enctype="multipart/form-data">
									@csrf
									{{ method_field("PATCH") }}	
									<button type="submit" class="btn btn-danger">Cancel</button>
								</form>
								<a href="" onclick="deleteOrder({{$order->id}})" class="btn btn-dark fas fa-trash-alt" data-toggle="modal" data-target="#delete_order"></a>
						</td>
						@endif				
					</tr>
 					@endforeach
				</tbody> 					
			</table>
		</div> {{-- end col --}}
		</div>
	</div>
</div>

{{-- modal for delete order --}}
<div id="delete_order" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-bg">
      <div class="modal-header">
        <h5 class="modal-title">Delete Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="delete_order_question"> question from js ?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light"data-dismiss="modal">Cancel</button>
        <form id="delete-order-form" action="" method="POST">
          @csrf
          {{ method_field("DELETE") }}
          <button type="submit" class="btn btn-danger">Confirm Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>
{{-- end  modal for delete order --}}
@endsection                                                 

