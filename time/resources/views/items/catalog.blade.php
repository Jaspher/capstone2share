@extends('layouts.app')
@section('content')
<div class="container catalog-cont">

  @if(Session::has("session_addtocart"))
  <p id="success" class="flash-addtocart">{{ Session::get("session_addtocart") }}</p>
  @endif
  @if((Session::has("session_welcome"))&&(auth()->user()->is_admin == 1))
  <p id="success" class="welcome-admin text-center">{{ Session::get("session_welcome") }}</p>
  @elseif((Session::has("session_welcome")))
  <p id="success" class="welcome-user text-center">{{ Session::get("session_welcome") }}</p>
  @endif
  <div class="row">
    <div class="col-md-2 filter-cont p-1 m-0">
      
      @auth
      @if(auth()->user()->is_admin == 1)
      <a class="btn btns btn-addnewitem text-left mt-2"data-toggle="modal" data-target="#addItem" href="{{ url('/addItem') }}">ADD NEW ITEM</a>
      @endif
      @endauth
      <hr>
          
      <h3 class="text-left  filter-head">WATCHES</h3>
      <hr class="mt-1 pt-0">
      <a class="btn btns m-0 btn-block text-left" href="/catalog">Show All</a>
      <ul class="ulist">
        @foreach(\App\Category::all() as $category)
        <li >
          <a class="btn btns m-0 btn-block text-left" href="/menu/categories/{{ $category->id }}">{{ $category->name }}</a>
        </li>
        @endforeach
      </ul>      
      <hr class="m-1">
      <h3 class="filter-head">Brands</h3>
      <hr class="m-1">
      <ul class="ulist">
        @foreach(\App\Brand::all() as $brand)
        <li>
          <a class="btn btns m-0 btn-block text-left" href="/menu/brands/{{ $brand->id }}">{{ $brand->name }}</a>
        </li>
        @endforeach
      </ul>            
      <hr class="m-1">
      <h3 class="filter-head">Men | Women</h3>
      <hr class="m-1">
      @foreach(\App\Gender::all() as $gender)
      <a class="btn btns m-0 btn-block text-left" href="/menu/gender/{{ $gender->id }}">{{ $gender->name }}</a>
      @endforeach
    </div> {{-- end col --}}
    <div class="col-md-10">
      <div class="row">
        @foreach($items as $indiv_item)
        <div class="col-md-3  p-1">
          <div class="card card-bg text-left">
            @if($indiv_item->stocks != 0)
            <img src="/{{$indiv_item->image_url}}" alt="" class="card-img-top catalog-img">
            @else
            <div class="out-stock-pos">
            <img src="/{{$indiv_item->image_url}}" alt="" class="card-img-top catalog-img out-stock">
            <p class="p-out-stock">OUT OF STOCK</p>
            </div>
            @endif
            <div  class="card-body p-0">
              <p class="card-title item-name m-0">{{$indiv_item->name}}</p>
              <hr class="m-0">
              <p class="item-price">₱{{number_format($indiv_item->price,2)}}</p>
              @auth
              @if(auth()->user()->is_admin == 1)
              <p class="cat-stocks">{{$indiv_item->stocks}} Left</p>
              @endif
              @endauth
              @auth
              @if(($indiv_item->stocks < 20)&&($indiv_item->stocks != 0)&&(auth()->user()->is_admin != 1))
              <p class="cat-hot fab fa-hotjar"></p>
              @endif
              @endauth
              <div id="add-cart" class="add-to-cart">
                <form action="/addToCart/{{ $indiv_item->id }}" method="POST">
                  @csrf
                  <input type="hidden" name="name" value="{{$indiv_item->name}}">
                  <input type="number" min="0" max="{{$indiv_item->stocks}}" name="quantity" class="text-center mb-1 qty-input" placeholder="0">
                  
                  @if((Auth::user() != null)&&(auth()->user()->is_admin != 1))
                  <button  type="submit" class="btn btn-block btn-outline-dark fas fa-shopping-cart sub-input text-center"></button>
                  @else
                  <a href="{{ url('/login') }}" class="btn btn-block btn-outline-dark fas fa-shopping-cart sub-input text-center"></a>
                  @endif
                </form>
              </div>
            </div> {{-- end card body --}}
            <input type="hidden" class="edit_name" data-id="{{$indiv_item->id}}" value="{{$indiv_item->name}}">
            <input type="hidden" class="edit_desc" data-id="{{$indiv_item->id}}" value="{{$indiv_item->description}}">
            <input type="hidden" class="edit_price" data-id="{{$indiv_item->id}}" value="{{number_format($indiv_item->price,2)}}">
            <input type="hidden" class="edit_price2" data-id="{{$indiv_item->id}}" value="{{$indiv_item->price}}">
            <input type="hidden" class="edit_cat_id" data-id="{{$indiv_item->id}}" value="{{$indiv_item->category_id}}">
            <input type="hidden" class="edit_img" data-id="{{$indiv_item->id}}" value="{{$indiv_item->image_url}}">
            <input type="hidden" class="edit_gender" data-id="{{$indiv_item->id}}" value="{{$indiv_item->gender_id}}">
            <input type="hidden" class="edit_stocks" data-id="{{$indiv_item->id}}" value="{{$indiv_item->stocks}}">
            <input type="hidden" class="edit_brand" data-id="{{$indiv_item->id}}" value="{{$indiv_item->brand_id}}">
          </div>
          @auth
          @if(auth()->user()->is_admin == 1)
          <div class="card-footer">
            <button type="button"  class="btn btn-block btn-outline-dark viewBtn mt-2" data-id="{{$indiv_item->id}}" data-toggle="modal" data-target="#viewdetails">View Details</button>
          </div>
          @endif
          @endauth
        </div> {{-- end col 3 --}}
        @endforeach
      </div>
    </div> {{-- end col 9--}}
  </div>  {{--  end  main row --}}
</div>    {{--  end cont --}}


{{-- modal for view details --}}
<div id="viewdetails" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-bg">
      <div class="modal-header">
        <h5 id="view-name" class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h2 id="view-desc"></h2>
        <p id="view-price"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-muted" data-dismiss="modal">Cancel</button>
        <button type="button" data-toggle="modal"  data-target="#deleteConf" data-dismiss="modal" class="btn btn-danger">Delete</button>
        <button  type="button" data-toggle="modal"  data-target="#editItem"  class="btn btn-primary">Edit</button>
      </div>
    </div>
  </div>
</div>
{{-- end modal for view details --}}
{{-- modal for detete item confirmation --}}
<div id="deleteConf" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content modal-bg">
      <div class="modal-header">
        <h5 class="modal-title">Delete Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="delete-name">delete message from js</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light"data-dismiss="modal">Cancel</button>
        <form id="delete-form" action="" method="POST">
          @csrf
          {{ method_field("DELETE") }}
          <button type="submit" class="btn btn-danger">Confirm</button>
        </form>
      </div>
    </div>
  </div>
</div>
{{-- end modal for delete item confirmation --}}
{{-- modal for edit items --}}
<div id="editItem" class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalScrollableTitle">Edit Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- edit form  --}}
        <div class="container text-center">
          <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
              {{--  <h1>Edit Item</h1> --}}
              
              <form id="edit-form" action="" method="POST" class="form-group form-font" enctype="multipart/form-data">
                @csrf
                {{ method_field("PATCH") }}
                <div class="form-group">
                  <label id="" for="name">Item Name</label>
                  <input id="edit-name" type="text" name="name" value="" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="description">Description</label>
                  <input id="edit-description" type="text" name="description" value="" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="price">Price</label>
                  <input id="edit-price2" type="number" name="price" value="" class="form-control" min="0" required>
                </div>
                <div class="form-group">
                  <label  for="stocks">Stocks</label>
                  <input id="edit-stocks" type="number" name="stocks" placeholder="Input Stocks" class="form-control" min="1" required>
                </div>
                <div class="form-group">
                  <div>
                    <label  for="image">Image</label> <br>
                    <img id="edit-image" class="editform-img edit-form-img" src="" alt="">
                    <input type="file" id="image" name="image" class="form-control" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="category">Category</label>
                  <select name="category" id="edit-category" class="form-control">
                    @foreach ($categories as $category)
                    <option  value="{{$category->id}}">
                    {{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label  for="brand">Brands</label>
                  <select name="brand" id="edit-brand" class="form-control">
                    @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label  for="gender">Gender</label>
                  <select name="gender" id="edit-gender" class="form-control">
                    @foreach($genders as $gender)
                    <option value="{{$gender->id}}">{{$gender->name}}</option>
                    @endforeach
                  </select>
                </div>
                <button type="submit" class="btn btn-outline-dark btn-block mt-4">Submit</button>
              </form>
              
            </div>
          </div>
        </div>
        {{-- end edit form --}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
        
      </div>
    </div>
  </div>
</div>
{{-- end modal for edit items --}}
{{-- modal for add items --}}
<div id="addItem" class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalScrollableTitle">Add Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{-- add item form --}}
        <div class="container text-center">
          <div class="row">
            <div class="col-md-10 offset-1">
              <form action="/menu/add" method="POST" class="form-group" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label  for="name">Item Name</label>
                  <input type="text" name="name" placeholder="Input Item" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="description">Description</label>
                  <input type="text" name="description" placeholder="Input description" class="form-control" required>
                </div>
                <div class="form-group">
                  <label  for="price">Price</label>
                  <input type="number" name="price" placeholder="Input Price" class="form-control" min="0" required>
                </div>
                <div class="form-group">
                  <label  for="stocks">Stocks</label>
                  <input type="number" name="stocks" placeholder="Input Stocks" class="form-control" min="1" required>
                </div>
                <div class="form-group">
                  <label  for="image">Image</label>
                  <input type="file" id="image" name="image" class="form-control" required>
                </div>
                <div class="form-group">
                  <label  for="category">Category</label>
                  <select name="category" id="category" class="form-control">
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label  for="brand">Brands</label>
                  <select name="brand" id="brand" class="form-control">
                    @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label  for="gender">Gender</label>
                  <select name="gender" id="gender" class="form-control">
                    @foreach($genders as $gender)
                    <option value="{{$gender->id}}">{{$gender->name}}</option>
                    @endforeach
                  </select>
                </div>
                <button type="submit" class="btn btn-outline-primary btn-block mt-4">Submit</button>
              </form>
            </div>
          </div>
        </div>
        {{-- end add item form --}}
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
{{-- end modal for add items --}}
@endsection