@extends('layouts.app')
@section('content')
{{-- {{ dd(Session::get('cart')) }} --}}
<div class="row m-0">
	<div class="col-md-8 offset-2">
		<div class="details cart_content_bg mt-4">
			<h1 class="text-center d-block cart-title">Cart</h1>
			@if(Session::has('cart') != [])
			<table class="table table-hover text-center cart-table">
				<thead>
					<th>Image</th>
					<th>Item</th>
					<th class="th-qty">Quantity</th>					
					<th>Price</th>
					<th>Subtotal</th>
					<th>Action</th>
				</thead>
				<tbody>
					@foreach($item_cart as $item)
					<tr>
						<td>
							<img class="cart-img" src="/{{ $item->image_url }}" alt="">	
						</td>
						<td>{{ $item->name }}</td>
						 {{-- <td>{{ $item->stocks }}</td>	 --}}
						<td>

							<form  method="post" id="update_quantity{{$item->id}}">
									@csrf
									{{ method_field('PATCH') }}
									<div class="input-group">
										<div class="input-group-prepend pl-4">
										@if($item->quantity != 1)
											<button  type="button" class="input-group-text round-0 btn-plus-minus"  onclick="minus({{ $item->id }})">&#8722;</button>
										@else
										<button  type="button" class="input-group-text round-0 btn-plus-minus-disabled" disabled>&#8722;</button>
										@endif
										</div>										
										<input type="number" min="1" max="{{ $item->stocks }}" value="{{ $item->quantity }}" name="quantity" onchange="qty_update({{$item->id}})" class="cart-input text-right w-10" id="quantity{{$item->id}}">
										<div class="input-group-append">
											@if($item->stocks > $item->quantity)
											<button type="button" class="input-group-text round-0 btn-plus-minus" onclick="plus({{ $item->id }})">+</button>
											@else
											<button type="button" class="input-group-text round-0 btn-plus-minus btn-plus-minus-disabled" disabled>+</button>
											@endif
										</div>
									</div>
								</form>
						</td>
						<td>₱{{ number_format($item->price,2) }}</td>
						<td>₱{{ number_format($item->subtotal,2) }}</td>
						<td>
							<button class="btn btn-danger"onclick="openDeleteModal('{{ $item->id }}','{{ $item->name }}')" data-toggle="modal" data-target="#delete-modal">Remove Item</button>
						</td>
					</tr>
					@endforeach
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<th>Total:</th>
						<th>₱{{number_format($total,2)}}</th>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>						
						<td><a onclick="pay()"  class="btn btn-success">Check out</a></td>
						<td>
							
						<button class="btn btn-danger " data-toggle="modal" data-target="#empty-modal" type="submit">Empty Cart</button>
						</td>
					</tr>
				</tbody>				
			</table>
			@else
			<h3 class="text-center no-item">No Items.</h3>
			@endif
			<div class="modal fade text-dark" tabindex="-1" role="dialog" id="delete-modal">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Delete Item</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body ">
							<p id="delete_question">Are you sure you want to delete ?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<form id="delete_form" method="POST">
								@csrf
								{{method_field('DELETE')}}
								
								<button type="submit" class="btn btn-danger fas fa-trash-alt">Delete</button>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade text-dark" tabindex="-1" role="dialog" id="empty-modal">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Empty Cart</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body ">
							<p>Are you sure you want to empty your Cart ?</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<form action="{{ url('/cart/clearCart') }}" method="POST">
								@csrf
								{{method_field('DELETE')}}
								<button class="btn btn-danger" type="submit">Empty Cart</button>
							</form>
						</div>
					</div>
				</div>
			</div>	



	@if(Session::has('cart'))
	<script src="https://js.stripe.com/v3/"></script>
	<script>
		
		const stripe = Stripe('pk_test_KCtD6izaShN11osHZJ2k2sUQ00WkuDYDAJ');
		function pay(){
			// alert('Connected');
			stripe.redirectToCheckout({
			sessionId:'{{$CHECKOUT_SESSION_ID}}'
			}).then((result) => {
			});
		}
	</script>
	@endif
@endsection