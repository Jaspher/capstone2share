 <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>inTIME</title>
    <link rel="icon" class="tab-logo" href="{{asset('images/intime-logo.png')}}">
   

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Heebo|Hind+Madurai|Patua+One|Poppins:300|Quicksand:700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli|Rubik&display=swap" rel="stylesheet">
  <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>  
    <!-- Styles -->
    
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">  
</head>
<body>

    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light shadow-sm sticky-top nav-bg p-0">
            <div class="container">
                <a class="navbar-brand web-brand" href="{{ url('/') }}">
                    <img class="intime-logo" src="{{asset('images/intime-logo.png')}}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link web-brand-link {{Request::segment(1) === 'catalog' ? 'active' : null}}" href="{{ url('/catalog') }}">{{ __('CATALOG') }}                                
                            </a>
                        </li>
                        @auth
                         <li class="nav-item">
                            <a class="nav-link web-brand-link {{Request::segment(1) === 'orders' ? 'active' : null}}" href="{{ url('/orders') }}">{{ __('ORDERS') }}                                
                            </a>
                        </li>
                        @if(auth()->user()->is_admin == 1)                      
                        <li class="nav-item">
                            <a class="nav-link web-brand-link {{Request::segment(1) === 'user_list' ? 'active' : null}}" href="{{ url('/user_list') }}">{{ __('USERS') }}                                
                            </a>
                        </li>
                        @endif
                        @endauth                                            
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @auth                        
                        <li class="nav-item">
                            <a class="nav-link web-brand-link fas fa-shopping-cart cart-icon {{Request::segment(2) === 'myCart' ? 'active' : null}}" href="{{ url('/menu/myCart') }}">{{ __('Cart') }}
                                @if(Session::has('cart'))
                                <span class="badge badge-dark ">{{ array_sum(Session::get('cart')) }}</span>
                                @endif
                            </a>
                        </li>
                        @endauth
                        @guest
                            <li class="nav-item">
                                <a class="nav-link web-brand-link {{Request::segment(1) === 'login' ? 'active' : null}} " href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                           
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link web-brand-link {{Request::segment(1) === 'register' ? 'active' : null}}" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle web-brand-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  <img class="user-img mr-2" src="/{{Auth::user()->image_url}}">{{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right logout-bg" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item logout-bg-font" href="/profile">
                                       
                                      {{ __('Profile') }}
                                    </a> 
                                    <a class="dropdown-item logout-bg-font" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                           
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-2 main-bg">
            @yield('content')
        </main>      
    </div>

    <!-- Footer -->
<footer class="page-footer font-small cyan darken-3 footer-bg">

  <!-- Footer Elements -->
  <div class="container">

    <!-- Grid row-->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-12 pt-5">
        <div class="mb-5 text-center">

          <!-- Facebook -->
          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!-- Twitter -->
          <a class="tw-ic">
            <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!-- Google +-->
          <a class="gplus-ic">
            <i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!--Linkedin -->
          <a class="li-ic">
            <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!--Instagram-->
          <a class="ins-ic">
            <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!--Pinterest-->
          <a class="pin-ic">
            <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
          </a>
        </div>
      </div>
      <!-- Grid column -->

    </div>
    <h5 class="text-uppercase font-weight-bold text-center">Disclaimer</h5>
        <p class="text-center">No copyright infringement is intended. This is only for educational purposes and not for profit. Some asset/s used are not owned by the developer unless otherwise stated. The credit goes to the owner.</p>
    <!-- Grid row-->
    <p class="text-center">Web logo is designed by <a class="footer-link" href="https://www.facebook.com/KimAlojado">Kim Alojado.</a></p>
  </div>
  <!-- Footer Elements -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright |  JaspherDugang
  </div>
  <!-- Copyright -->

</footer>

</body>


 <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <script src="{{ asset('js/script.js') }}" defer></script>
</html>
