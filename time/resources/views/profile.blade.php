@extends('layouts.app')

@section('content')
{{-- {{dd($user->image_url)}} --}}
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 reg-form">
            <img class="avatar" src="/{{$user->image_url}}">
           <h2 class="mt-3">{{$user->name}}'s Profile</h2>
           <form enctype="multipart/form-data" action="/profile/save" method="POST">
               @csrf
               {{ method_field("PATCH") }}
               <div class="form-group">
                  <label id="" for="name">Update Profile Image</label>
                  <input  type="file" name="avatar" value="" class="form-control">
                </div>
                  <button type="submit" class="btn btn-outline-success mt-4">Submit</button>
           </form>
        </div> {{-- end col --}}
    </div>
</div>
@endsection
