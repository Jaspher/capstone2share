@extends('layouts.app')
@section('content')
<div class="container">
	<div class="jumbotron">
	<div class="row ">
	</div>
		<div class="col user_list_table">
			<table class="table table-hover details text-center  text-dark ">
				<h1 class="text-center user_list_h">LIST OF USERS</h1>
				<thead>					
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Role</th>						
						<th>Action</th>						
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{$user->name}}</td>
						<td>{{$user->email}}</td>
						<td>
							@if($user->is_admin == 1)  
							Admin
							@else
							Normal user
							@endif
						</td>
						<td>
							@if($user->email == 'jaspher@gmail.com')
							Super Admin
							@else
							<button class="btn-outline-dark btn d-inline" data-toggle="modal" data-target="#admin_modal" onclick="makeAdmin({{$user->id}},'{{$user->name}}')">Make Admin</button>

							<button class="btn-outline-danger btn d-inline" data-toggle="modal" data-target="#remove_admin_modal" onclick="makeUser({{$user->id}},'{{$user->name}}')">Remove as admin</button>
							@endif
						</td>										
					</tr>
					@endforeach
				</tbody>
		
			</table>
		</div>
	</div>
</div>
{{-- make admin  modal --}}
<div id="admin_modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change User Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="makeAdminQ"></p>
      </div>
      <div class="modal-footer">
      	<form id="makeAdminForm" method="POST" class="d-inline">
      		@csrf
      		{{method_field('PATCH')}}
        	<button type="submit" class="btn btn-primary">Confirm</button>
        </form>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
{{-- end make admin modal --}}

{{-- make user  modal --}}
<div id="remove_admin_modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Change User Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="makeUserQ"></p>
      </div>
      <div class="modal-footer">
      	<form id="makeUserForm" method="POST" class="d-inline">
      		@csrf
      		{{method_field('PATCH')}}
        <button type="submit" class="btn btn-primary">Confirm</button>        	
        </form>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>

{{-- end make user  modal --}}
@endsection                                                 