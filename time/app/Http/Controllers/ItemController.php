<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use \App\Brand;
use \App\Gender;
use \App\Order;
use \App\User;
use Session;
use Auth;

class ItemController extends Controller
{
    public function ShowItemsAddItems(){
        $items = Item::all();
        $categories = Category::all();    
        $brands = Brand::all();
        $genders = Gender::all();
        // dd($items);
        // dd($categories);
    	return view('items.catalog',compact('items','categories','brands','genders'));
    }

    public function saveItems(Request $request){
        $item = new Item;
        // dd($request);
        $item->name = $request->name;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->stocks = $request->stocks;
        $item->category_id = $request->category;
        $item->brand_id = $request->brand;
        $item->gender_id = $request->gender;
        $image = $request->file('image');
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $item->image_url=$destination . $image_name;
        $item->save();
        return redirect('/catalog');
    }

    public function saveEdit($id,Request $request){
        $item = Item::find($id);
        $categories = Category::all();    
        $item->name = $request->name;
        $item->description = $request->description;
        $item->price = $request->price;
        $item->stocks = $request->stocks;
        $item->category_id = $request->category;
        $item->brand_id = $request->brand;        
        $item->gender_id = $request->gender;
        
        if($request->file('image') != null){
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $item->image_url=$destination . $image_name;
        } 
       $item->save();
        return redirect('/catalog');
    }

    public function deleteItem($id,Request $request){
        $item = Item::find($id);
        // dd($item);
    	$item->delete();
    	return redirect('/catalog');
    }
    
    public function addToCart($id, Request $request){  
        $item = Item::find($id);
        // dd($item->stocks);
        if($item->stocks == 0){
            return redirect('/catalog');
        }  

        if(Session::has('cart')){
            $cart = Session::get('cart');
            
        }else{
            $cart=[];
        }
        // dd($request->quantity);
        // catch
        if(($request->quantity == NULL)||($request->quantity == 0)){
            $quantity = 1;
        }else{
            $quantity = $request->quantity;
        }
        // dd($cart[$id]);

        if(isset($cart[$id])) {
                      
            $cart[$id] +=  $quantity;  
        }else{
            
            $cart[$id] =  $quantity;             
        } 
        // catch
        if($cart[$id] > $item->stocks){
            $cart[$id] = $item->stocks;
            Session::put('cart',$cart); 
            Session::flash('session_addtocart',"$item->stocks $request->name successfully added to your cart"); 
            return redirect('/catalog');
        }else{
            
        Session::put('cart',$cart); 
        Session::flash('session_addtocart',"$quantity $request->name successfully added to your cart");    
        return redirect('/catalog');
        }
        
        // Session::forget('cart');     
        
    }

    public function showCart(){
        $item_cart = [];
        $total = 0;
        $line_items =[];
        $user = Auth::user();
        if(Session::has('cart')) {
            $cart = Session::get('cart');
            // dd($cart);
            foreach ($cart as $id => $quantity) {
                $item = Item::find($id);
                // dd($item->stocks);
                if($quantity > $item->stocks){
                    $quantity = $item->stocks;
                }

                $item->quantity = $quantity;
                $item->subtotal = $item->price * $quantity;                
                $total += $item->subtotal;
                $item_cart[] = $item;

                $line_items[] = [
                    'name' => $item->name,
                    'description' => $item->description,
                    'images' => [$item->image_url],
                    'amount' => str_replace(".","",$item->price),
                    'currency' => 'PHP',
                    'quantity' => (int)$quantity,
                  ];
            }
            \Stripe\Stripe::setApiKey('sk_test_DDPFs5rEuHqwNSmcUinKUxCZ00bhao5JKL');

                        $stripe_session = \Stripe\Checkout\Session::create([
                          
                          'payment_method_types' => ['card'],
                          'customer_email'=>$user->email,
                          'line_items' =>$line_items,                                                    
                          'success_url' => 'http://fierce-mountain-88902.herokuapp.com/transaction_complete',
                          'cancel_url' => 'http://fierce-mountain-88902.herokuapp.com/menu/myCart',
                        ]);
                        // dd($stripe_session);
                         $CHECKOUT_SESSION_ID = $stripe_session['id'];
                          return view('items.cart_content',compact('item_cart','total','CHECKOUT_SESSION_ID'));
            
        }
            return view ('items.cart_content',compact('total','item_cart'));
            
    }

    public function checkout(){
        $order = new Order;
        $order->user_id = Auth::user()->id;
        $order->total = 0; //set initial value 0
        $order->status_id = 1;
        $total = 0;
        $order->save();
        
        // dd(Session::get('cart'));
        foreach (Session::get('cart') as $item_id => $quantity) {
            
            $order->items()->attach($item_id,['quantity'=>$quantity]);
            $item = Item::find($item_id);
            $total += $item->price * $quantity;
            // dd($item);
            $item->stocks = $item->stocks-$quantity;
            $item->save();
            
        }

        $order->total=$total;        
        $order->save();
       
        Session::forget('cart');

        // return redirect('/catalog');
        return view('orders.order_confirmation',compact('order'));
        
   }
   public function showOrders(){
    if(auth()->user()->is_admin == 1){
        $orders = Order::all();
    }else{
        
    $user = Auth::user();
    // dd($user->id);
    $orders = Order::where('user_id', $user->id)->get();
    }
    // $users = User::all();
    // dd($orders);
    return view('orders.order_history',compact('orders'));
   }

    public function removeCartItem ($id){
         $item = Item::find($id);
        Session::forget("cart.$id");

        if(Session::get('cart') != []) {

        return back();

        }else{
            Session::forget('cart');            
            return back();
        } 
    }

    public function  clearCart(){
        Session::forget('cart');
        // $items = Item::all();
        return redirect('/catalog');
    }


    public function changeItemQty($id, Request $request){
        $item = Item::find($id);

        $cart = session::get('cart');
        $cart[$id] = $request->quantity;
        // dd($item->stocks);
        // dd($cart[$id]);
        // dd($request->quantity);

        // if qty on cart blade is negative
        if($cart[$id] < 0){
            $cart[$id] = 1;            
        }

        //if qty on cart blade is greater than the value of stocks
        if($item->stocks < $cart[$id]){
            $cart[$id] = $item->stocks;
        }
        // dd($request->quantity);
        session::put('cart', $cart);
          return back();
    }


    public function showAllUsers(){
        $users = User::all();
        return view('users.user_list',compact('users'));
    }

    public function makeAdmin($id){
        $users = User::find($id);
        $users->is_admin = 1;
        $users->save();
        return redirect ('/user_list');
    }

       public function makeUser($id){
        $users = User::find($id);
        $users->is_admin =0;
        $users->save();
        return redirect ('/user_list');
    }
}
