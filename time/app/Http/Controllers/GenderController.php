<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Item;
use \App\Brand;
use \App\Gender;

class GenderController extends Controller
{
    public function findItems($id){
    	$gender = Gender::find($id);
    	// dd($category);
        $items = $gender->items;
        
        $categories = Category::all();    
        $brands = Brand::all();
        $genders = Gender::all();
        // dd($items);
    	return view('items.catalog',compact('items','categories','brands','genders'));
   }	
}
