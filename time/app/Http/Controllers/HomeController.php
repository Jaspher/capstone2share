<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Auth;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
         $username = auth::user()->name;        
        if(auth()->user()->is_admin == 1){

        Session::flash('session_welcome',"Welcome Admin $username !"); 
        }else{
        Session::flash('session_welcome',"Welcome $username! Enjoy your shopping!!"); 
        }

        return redirect('/catalog');
    }

    public function admin()

    {

    return view('admin');

    }
}
