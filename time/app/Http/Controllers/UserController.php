<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\User;
class UserController extends Controller
{
    public function profile(){
    	return view('profile',array('user' => Auth::user()));
    }

    public function updateAvatar(Request $request){
    	$user = Auth::user();
	 	$avatar = $request->file('avatar');
        $filename = time().".".$avatar->getClientOriginalExtension();
        $destination = "images/";
        $avatar->move($destination, $filename);
        $user->image_url=$destination.$filename;
    	// dd($avatar);
    	$user->save();
    	return view('profile',array('user' => Auth::user()));
    }
}
