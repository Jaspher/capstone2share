<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Item;
use \App\Brand;
use \App\Gender;

class BrandController extends Controller
{
    public function findItems($id){
    	$brand = Brand::find($id);
    	// dd($category);
        $items = $brand->items;
        
        $categories = Category::all();    
        $brands = Brand::all();
        $genders = Gender::all();
        // dd($items);
    	return view('items.catalog',compact('items','categories','brands','genders'));
   }	
}
