<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \App\Order;
use \App\Status;
use Session;

class OrderController extends Controller
{
    public function cancelOrder($id) {
        $order = Order::find($id);
        $statuses = Status::all();
        // dd($order->status_id);
        if($order->status_id == 3) {
            return redirect("/orders");
        }else{            
        $order ->status_id = 2;
        $ordercode = strtotime($order->created_at);
        $order->save();
        }
      
        return redirect("/orders");
    }

    public function pendingOrder($id) {
        $order = Order::find($id);
        $statuses = Status::all();
        $order ->status_id = 1;
        $ordercode = strtotime($order->created_at);
        $order->save();
        return redirect("/orders");
    }

    public function completedOrder($id) {
        $order = Order::find($id);
        $statuses = Status::all();
        $order ->status_id = 3;
        $ordercode = strtotime($order->created_at);
        $order->save();
        return redirect("/orders");
    }

    public function deleteOrder($id){
        $order = Order::find($id);
        $order->delete();
        return back()
        ->with('delete_message',"Order #$id has been succesfully deleted")
        ->with('undo_url',"/restore_order/$id");
    }

    public function restoreOrder($id) {
        $order = Order::onlyTrashed()->where('id',$id)->first();
        $order->restore();
        Session::flash('restore_message',"Order #$order->id has been restored");

        return back();
    }
}
