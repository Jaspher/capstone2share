// alert('Connected')
let viewBtns = document.querySelectorAll(".viewBtn");
let names = document.querySelectorAll(".edit_name");
let descriptions = document.querySelectorAll(".edit_desc");
let prices = document.querySelectorAll(".edit_price");
let prices2 = document.querySelectorAll(".edit_price2");
let categories_id = document.querySelectorAll(".edit_cat_id");
let images = document.querySelectorAll(".edit_img");
let brands_id = document.querySelectorAll(".edit_brand");
let gender_id = document.querySelectorAll(".edit_gender");
let stocks = document.querySelectorAll(".edit_stocks");


window.addEventListener('load', function() {
        if (document.querySelector('#success').innerHTML == "") {
            console.log(document.querySelector("#success"));
        } else {
            document.querySelector("#success").classList.add('hide-success');
        }
  });  

function makeAdmin(id,name){
    document.querySelector('#makeAdminForm').setAttribute('action','/user/'+id+'/makeAdmin');
    console.log(name);
    document.querySelector('#makeAdminQ').innerHTML = "Are you sure you want to make " + name +" an Admin?";
}

function makeUser(id,name){
    document.querySelector('#makeUserForm').setAttribute('action','/user/'+id+'/makeUser');
     console.log(name);
    document.querySelector('#makeUserQ').innerHTML = "Are you sure you want to make " + name +" a Normal user?";
}





function qty_update(id) {
   // console.log(id);
   if(document.querySelector("#quantity"+id).value == 0){
    document.querySelector("#quantity"+id).value = 1;
    document.querySelector("#update_quantity"+id).setAttribute('action','/mycart/'+id+'/changeQty');
   document.querySelector("#update_quantity"+id).submit();
   }else{

   document.querySelector("#update_quantity"+id).setAttribute('action','/mycart/'+id+'/changeQty');
   document.querySelector("#update_quantity"+id).submit();
   }
 
}


function deleteOrder(orderid){
    let question = document.querySelector("#delete_order_question");
    question.innerHTML = "Do you want to delete #" + orderid + "?";

    document.querySelector("#delete-order-form").setAttribute('action',"/delete_order/"+ orderid);
}


function openDeleteModal(id,name) {
    $('#delete_form').attr('action', '/mycart/' + id + '/delete');
    $('#delete_question').html("Do you want to delete " + name + "?");
    console.log(name);
  }

  function minus(id){ 
    // e.preventDefault();
    $value = $("#quantity"+id).val();
    $value = parseInt($value);
    if(isNaN($value)){
        $value = 2;
    }
    if($value > 1) {
      $("#quantity"+id).val($value-1);
    }
    $('#update_quantity'+id).attr('action', '/mycart/'+id+'/changeQty');
    console.log($('#update_quantity'+id));
    $('#update_quantity'+id).submit();
  }

  function plus(id){
    $value = $("#quantity"+id).val();
    $value = parseInt($value);
    if(isNaN($value)){
        $value = 1;
    }
    // console.log(id);
    $("#quantity"+id).val($value+1);
    $('#update_quantity'+id).attr('action', '/mycart/'+id+'/changeQty');
    $('#update_quantity'+id).submit();
  }


if(viewBtns){
    viewBtns.forEach(function(viewBtn){
        viewBtn.addEventListener('click',function(e){
            let id = e.target.dataset.id;
            console.log(id);

            descriptions.forEach(function(desc){
                    if(desc.dataset.id == id){
                        let mydesc = desc.value;
                        console.log(mydesc);
                        document.querySelector("#edit-description").setAttribute('value',mydesc);
                        document.querySelector("#edit-form").action="/menu/"+id+"/edit";
                        document.querySelector("#delete-form").action="/menu/"+id+"/delete";
                        document.querySelector("#view-desc").innerHTML = mydesc;
                        
                    }
            })
            
            names.forEach(function(name){
                if(name.dataset.id == id){
                    let myname = name.value;
                    console.log(myname);
                    document.querySelector("#edit-name").setAttribute('value',myname);
                    document.querySelector("#delete-name").innerHTML= "Are you sure you want to delete "+ myname + "?"
                    document.querySelector("#view-name").innerHTML = myname;
                    
                }
            })
            images.forEach(function(image){                
                if(image.dataset.id == id){
                    let myimage = "/"+ image.value;                    
                    console.log(myimage);
                    document.querySelector("#edit-image").src=myimage;
                }
            })

            prices.forEach(function(price){
                if(price.dataset.id == id){
                    let myprice = price.value;                
                    console.log(myprice);                    
                    document.querySelector("#view-price").innerHTML = "₱"+myprice;
                }
            })

            prices2.forEach(function(price2){
                if(price2.dataset.id == id){
                    let myprice2 = price2.value;                
                    console.log(myprice2);
                    document.querySelector("#edit-price2").setAttribute('value',myprice2);
                    
                }
            })

            stocks.forEach(function(stock){
                if(stock.dataset.id == id){
                    let mystock = stock.value;                
                    console.log(mystock);
                    document.querySelector("#edit-stocks").setAttribute('value',mystock);
                    
                }
            })

            categories_id.forEach(function(category_id){
                if(category_id.dataset.id == id){
                    let mycategory_id = parseInt(category_id.value);
                    let cat_id = mycategory_id - 1;
                    console.log(cat_id);
                    document.querySelector("#edit-category").options.selectedIndex = cat_id;
                }
            })

            brands_id.forEach(function(brand_id){
                if(brand_id.dataset.id == id){
                    let mybrand_id = parseInt(brand_id.value);
                    let brnd_id = mybrand_id - 1;
                    console.log(brnd_id)
                    document.querySelector("#edit-brand").options.selectedIndex = brnd_id;
                }
            })

            gender_id.forEach(function(genders_id){
                if(genders_id.dataset.id == id){
                    let mygenders_id = parseInt(genders_id.value);
                    let gen_id = mygenders_id - 1;
                    
                    document.querySelector("#edit-gender").options.selectedIndex = gen_id;
                }
            })

        })
    })
    
   


}





    
