<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/catalog');
});

Route::get('/catalog', 'ItemController@ShowItemsAddItems');
Route::POST('/menu/add', 'ItemController@saveItems');
Route::patch('/menu/{id}/edit', 'ItemController@saveEdit');
Route::delete('/menu/{id}/delete', 'ItemController@deleteItem');

Route::POST('/addToCart/{id}', 'ItemController@addToCart');
Route::get('/menu/myCart', 'ItemController@showCart');
Route::delete('/mycart/{id}/delete', 'ItemController@removeCartItem');
Route::delete('/cart/clearCart', 'ItemController@clearCart');
Route::patch('/mycart/{id}/changeQty', 'ItemController@changeItemQty');
Route::get('/checkout', 'ItemController@checkout');
Route::get('/transaction_complete', 'ItemController@checkout');
Route::get('/orders', 'ItemController@showOrders');
Route::patch('/orders/{id}/cancel', 'OrderController@cancelOrder');
Route::patch('/orders/{id}/pending', 'OrderController@pendingOrder')->middleware('admin');
Route::patch('/orders/{id}/completed', 'OrderController@completedOrder')->middleware('admin');

Route::get('/menu/categories/{id}', 'CategoryController@findItems');
Route::get('/menu/brands/{id}', 'BrandController@findItems');
Route::get('/menu/gender/{id}', 'GenderController@findItems');

Route::delete('/delete_order/{id}', 'OrderController@deleteOrder');
Route::get('/restore_order/{id}', 'OrderController@restoreOrder');

Route::get('/profile', 'UserController@profile');
Route::patch('/profile/save', 'UserController@updateAvatar');

Route::patch('/user/{id}/makeAdmin', 'ItemController@makeAdmin');
Route::patch('/user/{id}/makeUser', 'ItemController@makeUser');

Route::get('/user_list', 'ItemController@showAllUsers');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/routes', 'HomeController@admin')->middleware('admin');
